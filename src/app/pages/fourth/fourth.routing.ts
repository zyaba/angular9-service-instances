import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FourthComponent} from './fourth.component';

export const FourthRoutes: Routes = [
	{
		path: 'fourth',
		loadChildren: () => import('./fourth.module').then((m) => m.FourthModule),
		data: {preload: true}
	}
];

const routes: Routes = [
	{
		path: '',
		component: FourthComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MainRoutingModule {
}
