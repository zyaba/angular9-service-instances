import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FourthComponent} from './fourth.component';
import {LinksModule} from '../../shared/components/links/links.module';
import {ProvidedInModuleProvidersService} from '../../shared/services/provided-in-module-providers-service';
import {MainRoutingModule} from './fourth.routing';
import {ComWithRootModule} from '../../shared/components/com-with-root/com-with-root.module';
import {ProvidedInTwoModulesService} from '../../shared/services/provided-in-two-modules-service';

@NgModule({
	declarations: [FourthComponent],
	imports: [
		CommonModule,
		MainRoutingModule,
		LinksModule,
		ComWithRootModule.forRoot()
	],
	providers: [ProvidedInModuleProvidersService, ProvidedInTwoModulesService]
})
export class FourthModule {
	constructor() {
		console.log('FourthModule constructor\n\n');
	}
}
