import {Component, OnInit} from '@angular/core';
import {ProvidedInRootService} from '../../shared/services/provided-in-root-service';
import {ProvidedInAppService} from '../../shared/services/provided-in-app-service';
import {ProvidedInComponentsService} from '../../shared/services/provided-in-components-service';
import {ProvidedInModuleProvidersService} from '../../shared/services/provided-in-module-providers-service';
import {ProvidedInTwoModulesService} from '../../shared/services/provided-in-two-modules-service';

@Component({
	selector: 'app-third',
	templateUrl: './fourth.component.html',
	styleUrls: ['./fourth.component.css'],
	providers: [ProvidedInComponentsService]
})
export class FourthComponent implements OnInit {

	constructor(private providedInRootService: ProvidedInRootService,
				private providedInAppService: ProvidedInAppService,
				private providedInComponentsService: ProvidedInComponentsService,
				private providedInModuleProvidersService: ProvidedInModuleProvidersService,
				private providedInTwoModulesService: ProvidedInTwoModulesService) {
	}

	ngOnInit(): void {
	}

}
