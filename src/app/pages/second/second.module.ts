import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SecondComponent} from './second.component';
import {MainRoutingModule} from './second.routing';
import {LinksModule} from '../../shared/components/links/links.module';
import {ProvidedInModuleProvidersService} from '../../shared/services/provided-in-module-providers-service';

@NgModule({
	declarations: [SecondComponent],
	imports: [
		CommonModule,
		MainRoutingModule,
		LinksModule
	],
	providers: [ProvidedInModuleProvidersService]
})
export class SecondModule {
	constructor() {
		console.log('SecondModule constructor\n\n');
	}

}
