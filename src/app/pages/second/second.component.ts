import {Component, OnInit} from '@angular/core';
import {ProvidedInRootService} from '../../shared/services/provided-in-root-service';
import {ProvidedInAppService} from '../../shared/services/provided-in-app-service';
import {ProvidedInComponentsService} from '../../shared/services/provided-in-components-service';
import {ProvidedInModuleProvidersService} from '../../shared/services/provided-in-module-providers-service';

@Component({
	selector: 'app-second',
	templateUrl: './second.component.html',
	styleUrls: ['./second.component.css'],
	providers: [ProvidedInComponentsService]
})
export class SecondComponent implements OnInit {

	constructor(private providedInRootService: ProvidedInRootService,
				private providedInAppService: ProvidedInAppService,
				private providedInComponentsService: ProvidedInComponentsService,
				private providedInModuleProvidersService: ProvidedInModuleProvidersService) {
	}

	ngOnInit(): void {
	}

}
