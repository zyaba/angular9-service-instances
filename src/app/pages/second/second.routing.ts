import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SecondComponent} from './second.component';

export const SecondRoutes: Routes = [
	{
		path: 'second',
		loadChildren: () => import('./second.module').then((m) => m.SecondModule),
		data: {preload: true}
	}
];

const routes: Routes = [
	{
		path: '',
		component: SecondComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MainRoutingModule {
}
