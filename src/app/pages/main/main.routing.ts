import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MainComponent} from './main.component';

export const MainRoutes: Routes = [
	{
		path: 'main',
		loadChildren: () => import('./main.module').then((m) => m.MainModule),
		data: {preload: true}
	}
];

const routes: Routes = [
	{
		path: '',
		component: MainComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MainRoutingModule {
}
