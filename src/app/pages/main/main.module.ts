import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainComponent} from './main.component';
import {MainRoutingModule} from './main.routing';
import {LinksModule} from '../../shared/components/links/links.module';
import {ProvidedInModuleProvidersService} from '../../shared/services/provided-in-module-providers-service';


@NgModule({
	declarations: [MainComponent],
	imports: [
		CommonModule,
		MainRoutingModule,
		LinksModule
	],
	providers: [ProvidedInModuleProvidersService]
})
export class MainModule {
	constructor() {
		console.log('MainModule constructor\n\n');
	}
}
