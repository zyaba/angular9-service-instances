import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ThirdComponent} from './third.component';
import {MainRoutingModule} from './third.routing';
import {LinksModule} from '../../shared/components/links/links.module';
import {ProvidedInModuleProvidersService} from '../../shared/services/provided-in-module-providers-service';

@NgModule({
	declarations: [ThirdComponent],
	imports: [
		CommonModule,
		MainRoutingModule,
		LinksModule
	],
	providers: [ProvidedInModuleProvidersService]
})
export class ThirdModule {
	constructor() {
		console.log('ThirdModule constructor\n\n');
	}
}
