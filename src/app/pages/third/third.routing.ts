import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ThirdComponent} from './third.component';

export const ThirdRoutes: Routes = [
	{
		path: 'third',
		loadChildren: () => import('./third.module').then((m) => m.ThirdModule),
		data: {preload: true}
	}
];

const routes: Routes = [
	{
		path: '',
		component: ThirdComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MainRoutingModule {
}
