import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {MainRoutes} from './pages/main/main.routing';
import {NgModule} from '@angular/core';
import {SecondRoutes} from './pages/second/second.routing';
import {ThirdRoutes} from './pages/third/third.routing';
import {FourthRoutes} from './pages/fourth/fourth.routing';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'main',
		pathMatch: 'full'
	},
	{
		path: '',
		component: AppComponent,
		children: [
			...MainRoutes,
			...SecondRoutes,
			...ThirdRoutes,
			...FourthRoutes
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {useHash: true})],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
