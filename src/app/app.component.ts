import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ProvidedInRootService} from './shared/services/provided-in-root-service';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	constructor() {
	}

	ngOnInit() {
	}
}
