import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LinksComponent} from './links.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [LinksComponent],
  exports: [
    LinksComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class LinksModule { }
