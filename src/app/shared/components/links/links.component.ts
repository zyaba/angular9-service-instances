import {Component, OnInit} from '@angular/core';
import {ProvidedInRootService} from '../../services/provided-in-root-service';
import {ActivatedRoute, Router} from '@angular/router';
import {MainComponent} from '../../../pages/main/main.component';
import {SecondComponent} from '../../../pages/second/second.component';
import {ThirdComponent} from '../../../pages/third/third.component';
import {FourthComponent} from '../../../pages/fourth/fourth.component';

@Component({
	selector: 'app-links',
	templateUrl: './links.component.html',
	styleUrls: ['./links.component.css'],
	providers: []
})
export class LinksComponent implements OnInit {
	public links = [
		'/main',
		'/second',
		'/third',
		'/fourth'
	];

	constructor(public service: ProvidedInRootService,
				public router: Router) {
	}

	ngOnInit(): void {
	}

}
