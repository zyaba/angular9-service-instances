import {Injectable} from '@angular/core';

@Injectable()
export class ProvidedInModuleProvidersService {
	constructor() {
		console.log('* 3. ProvidedInModuleProvidersService created *');
		console.log('- provided in every page module, no "providedIn: root" flag');
		console.log('- should be initialized only once with module load\n\n');
	}
}
