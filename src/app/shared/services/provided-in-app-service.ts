import {Injectable} from '@angular/core';

@Injectable()
export class ProvidedInAppService {
	constructor() {
		console.log('* 2. ProvidedInAppService created *');
		console.log('- provided in AppModule, no "providedIn: root" flag');
		console.log('- should be initialized only once\n\n');
	}
}
