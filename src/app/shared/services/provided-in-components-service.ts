import {Injectable} from '@angular/core';

@Injectable()
export class ProvidedInComponentsService {
	constructor() {
		console.log('* 4. ProvidedInComponentsService created *');
		console.log('- provided in every page component, no "providedIn: root" flag');
		console.log('- should be initialized on every component render\n\n');
	}
}
