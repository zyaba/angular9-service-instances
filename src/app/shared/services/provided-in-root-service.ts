import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class ProvidedInRootService {
	constructor() {
		console.log('* 1. ProvidedInRootService created *');
		console.log('- not provided anywhere, but has "providedIn: root" flag');
		console.log('- should be initialized only once\n\n');
	}
}
