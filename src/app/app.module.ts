import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routes';
import {ProvidedInAppService} from './shared/services/provided-in-app-service';
import {LinksModule} from './shared/components/links/links.module';

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		AppRoutingModule,
		LinksModule,
	],
	providers: [ProvidedInAppService],
	bootstrap: [AppComponent]
})
export class AppModule {
}
